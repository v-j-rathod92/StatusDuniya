package com.lim.statusduniya;

import android.app.Application;

import com.lim.statusduniya.storage.DatabaseUtil;

/**
 * Created by Lenovo on 04-02-2018.
 */

public class StatusDuniyaApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseUtil.init(this, "status duniya.sql", 1, null);
    }
}
