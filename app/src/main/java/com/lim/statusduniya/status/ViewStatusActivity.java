package com.lim.statusduniya.status;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lim.statusduniya.R;
import com.lim.statusduniya.model.Status;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rgi-40 on 5/2/18.
 */

public class ViewStatusActivity extends AppCompatActivity {

    public static final String STATUS_LIST = "status_list";
    public static final String SELECTED_POSITION = "selected_position";
    private ViewPager viewPager;

    private ArrayList<Status> list;
    private int selectedPosition;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_view_status);

        init();
    }

    private void init() {
        list = getIntent().getParcelableArrayListExtra(STATUS_LIST);
        selectedPosition = getIntent().getIntExtra(SELECTED_POSITION, 0);

        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(new StatusPagerAdapter());
        viewPager.setCurrentItem(selectedPosition);
    }

    class StatusPagerAdapter extends PagerAdapter {
        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            View view = inflater.inflate(R.layout.layout_status, null);

            TextView txtStatus = (TextView) view.findViewById(R.id.txtStatus);
            ImageView imgShare = (ImageView) view.findViewById(R.id.imgShare);
            ImageView imgCopy = (ImageView) view.findViewById(R.id.imgCopy);
            ImageView imgWhatsAppShare = (ImageView) view.findViewById(R.id.imgWhatsAppShare);
            ImageView imgFbShare = (ImageView) view.findViewById(R.id.imgFbShare);

            txtStatus.setText(list.get(position).getText());

            imgWhatsAppShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareStatusOnWhatsApp(list.get(position).getText());
                }
            });

            imgFbShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareStatusOnFb(list.get(position).getText());
                }
            });

            imgShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareStatus(list.get(position).getText());
                }
            });

            imgCopy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    copyStatus(list.get(position).getText());
                }
            });

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    private void shareStatusOnWhatsApp(String status) {
        if (checkAppInstall("com.whatsapp")) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.setPackage("com.whatsapp");
            sharingIntent.setAction(Intent.ACTION_SEND);
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, status);
            startActivity(sharingIntent);
        } else {
            showToast("Whatsapp is not installed on your device");
        }
    }

    private void shareStatusOnFb(String status) {
        boolean isAppInstalled = checkAppInstall("com.facebook.katana");
        if (isAppInstalled) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.setPackage("com.facebook.katana");
            sharingIntent.setAction(Intent.ACTION_SEND);
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, status);
            startActivity(sharingIntent);
        } else {
            showToast("Facebook app is not installed on your device");
        }
    }

    private void shareStatus(String status) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, status);
        startActivity(Intent.createChooser(sharingIntent, "Share Status"));
    }

    private void copyStatus(String status) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("status", status);
        clipboard.setPrimaryClip(clip);

        Toast.makeText(this, "Status copied", Toast.LENGTH_LONG).show();
    }

    private boolean checkAppInstall(String uri) {
        final PackageManager packageManager = getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(uri);
        if (intent == null) {
            return false;
        }
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return !list.isEmpty();
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}