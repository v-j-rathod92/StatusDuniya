package com.lim.statusduniya.status;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lim.statusduniya.R;
import com.lim.statusduniya.model.Status;
import com.lim.statusduniya.status.dummy.DummyContent.DummyItem;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private ArrayList<Status> mValues;
    private Status status;
    private StatusListFragment statusListFragment;
    private Context mContext;
    private TypedArray ta;
    private ArrayList<Integer> listRandomColors = new ArrayList<>();
    private int lastColor;

    public MyItemRecyclerViewAdapter(Context mContext, ArrayList<Status> list, StatusListFragment statusListFragment) {
        mValues = list;
        this.mContext = mContext;
        this.statusListFragment = statusListFragment;
        ta = mContext.getResources().obtainTypedArray(R.array.colors);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        status = mValues.get(position);

        if (listRandomColors.size() >= (position + 1)) {
            holder.txtBar.setBackgroundColor(listRandomColors.get(position));
        } else {
            int color = pickRandomColor();
            listRandomColors.add(color);
            holder.txtBar.setBackgroundColor(color);
            lastColor = color;
        }
        holder.mContentView.setText(status.getText());
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusListFragment.onStatusClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView txtBar;
        public final TextView mContentView;
        private final LinearLayout linearLayout;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            txtBar = (TextView) view.findViewById(R.id.txtBar);
            mContentView = (TextView) view.findViewById(R.id.content);
            linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
        }
    }

    private int pickRandomColor() {
        int color = ta.getColor(new Random().nextInt(ta.length()), 0);
        while (color == lastColor) {
            color = pickRandomColor();
        }
        return color;
    }
}
