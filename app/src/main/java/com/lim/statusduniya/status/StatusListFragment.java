package com.lim.statusduniya.status;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lim.statusduniya.R;
import com.lim.statusduniya.dao.StatusDAO;
import com.lim.statusduniya.model.Status;

import java.util.ArrayList;

public class StatusListFragment extends Fragment {
    private static final String CATEGORY_ID = "cat_id";
    private String selectedCategoryId;
    private ArrayList<Status> listStatus;

    public static StatusListFragment newInstance(String cat_id) {
        StatusListFragment fragment = new StatusListFragment();
        Bundle args = new Bundle();
        args.putString(CATEGORY_ID, cat_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            selectedCategoryId = getArguments().getString(CATEGORY_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        listStatus = StatusDAO.getStatusByCategoryId(selectedCategoryId);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new MyItemRecyclerViewAdapter(getActivity(), listStatus, this));
        }
        return view;
    }

    public void onStatusClick(int position) {
        Intent intent = new Intent(getActivity(), ViewStatusActivity.class);
        intent.putParcelableArrayListExtra(ViewStatusActivity.STATUS_LIST, listStatus);
        intent.putExtra(ViewStatusActivity.SELECTED_POSITION, position);
        startActivity(intent);
    }
}
