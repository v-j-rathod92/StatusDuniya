package com.lim.statusduniya.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.lim.statusduniya.model.Category;
import com.lim.statusduniya.model.Status;
import com.lim.statusduniya.storage.DatabaseUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by Lenovo on 04-02-2018.
 */

public class StatusDAO {
    private static final String GET_ALL_CATEGORY = "SELECT * FROM category";
    private static final String GET_CATEGORY_BY_ID = "SELECT * FROM status WHERE cat_id = ?";

    public static ArrayList<Category> getStatusCategories() {
        SQLiteDatabase db = null;
        Cursor c = null;
        ArrayList<Category> listCategories = new ArrayList<>();
        try {
            db = DatabaseUtil.getDatabaseInstance();
            c = db.rawQuery(GET_ALL_CATEGORY, null);

            while (c.moveToNext()) {
                Category category = new Category();
                category.setId(c.getString(c.getColumnIndex("cat_id")));
                category.setName(c.getString(c.getColumnIndex("name")));
                listCategories.add(category);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.closeResource(db, null, c);
        }
        return listCategories;
    }

    public static ArrayList<Status> getStatusByCategoryId(String cat_id) {
        SQLiteDatabase db = null;
        Cursor c = null;
        ArrayList<Status> listStatus = new ArrayList<>();
        try {
            db = DatabaseUtil.getDatabaseInstance();
            c = db.rawQuery(GET_CATEGORY_BY_ID, new String[]{cat_id});

            while (c.moveToNext()) {
                Status status = new Status();
                status.setCategoryId(c.getString(c.getColumnIndex("cat_id")));
                status.setText(c.getString(c.getColumnIndex("text")));
                listStatus.add(status);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.closeResource(db, null, c);
        }
        return listStatus;
    }
}
