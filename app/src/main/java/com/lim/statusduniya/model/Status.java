package com.lim.statusduniya.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by rgi-40 on 14/2/18.
 */

public class Status implements Parcelable {
    private String categoryId, text;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.categoryId);
        dest.writeString(this.text);
    }

    public Status() {
    }

    protected Status(Parcel in) {
        this.categoryId = in.readString();
        this.text = in.readString();
    }

    public static final Parcelable.Creator<Status> CREATOR = new Parcelable.Creator<Status>() {
        @Override
        public Status createFromParcel(Parcel source) {
            return new Status(source);
        }

        @Override
        public Status[] newArray(int size) {
            return new Status[size];
        }
    };
}
