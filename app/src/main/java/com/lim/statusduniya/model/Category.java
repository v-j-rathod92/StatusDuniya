package com.lim.statusduniya.model;

/**
 * Created by rgi-40 on 14/2/18.
 */

public class Category {
    private String id, name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
